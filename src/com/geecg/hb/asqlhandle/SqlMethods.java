package com.geecg.hb.asqlhandle;

import java.util.List;

import com.geecg.hb.bean.ConditionParams;

public interface SqlMethods<T, E> {
	/**
	 * 新增记录
	 */
	public void add(T t);
	/**
	 * 根据提供的参数返回查询对象
	 * @param obj
	 * @return
	 */
	public List<T> findByParams(T t);
	
	/**
	 * 根据提供的参数及查询条件返回查询对象
	 * @param obj
	 * @return
	 */
	public List<T> findByConditionParams(T t,ConditionParams...params);
	
	/**
	 * 更新对象，只更新有值的字段
	 * @param obj
	 */
	public void update(T t);
	
	/**
	 * 删除记录
	 * @param obj
	 */
	public void delete(T t);
	
	
}
