package com.geecg.hb.asqlhandle;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import com.geecg.hb.annotation.Column;
import com.geecg.hb.annotation.ID;
import com.geecg.hb.annotation.Ignore;
import com.geecg.hb.annotation.Table;
import com.geecg.hb.bean.ConditionParams;

public class SqlSplitHandle<T>{
	
	private Class<T> clazz;
	
	@SuppressWarnings("unchecked")
	public SqlSplitHandle(){
		ParameterizedType type =(ParameterizedType) this.getClass().getGenericSuperclass();
		try {
			clazz=(Class<T>) type.getActualTypeArguments()[0];
//			t=(T) Class.forName(type.getActualTypeArguments()[0].getTypeName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 获取要插入的字段名 
	 * */
	public String getInsertHandleSql(){
		StringBuilder sb=new StringBuilder();
		sb.append("insert into ");
		if(clazz.isAnnotationPresent(Table.class)){
			Table table=(Table) clazz.getAnnotation(Table.class);
			sb.append(table.table());
		}else{
			sb.append(clazz.getSimpleName());
		}
		Field[] fields=clazz.getDeclaredFields();
		sb.append("(");
		for (Field field : fields) {
			if(field.isAnnotationPresent(Ignore.class)){
					continue;
			}
			if(field.isAnnotationPresent(Column.class)){
				Column column=field.getAnnotation(Column.class);
				sb.append(column.name()+",");
			}else{
				field.setAccessible(true);
				sb.append(field.getName()+",");
				field.setAccessible(false);
			}
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append(")");
		return sb.toString();
	}
	
	/**
	 * 获取查询语句sql
	 * */
	public String getSelectHeaderSql(){
		StringBuffer sb=new StringBuffer();
		Field[] fields=clazz.getDeclaredFields();
		sb.append("select ");
		for (Field field : fields) {
			if(!field.isAnnotationPresent(Ignore.class)){
				if(field.isAnnotationPresent(Column.class)){
					Column column=field.getAnnotation(Column.class);
					sb.append(column.name()+",");
				}else{
					sb.append(field.getName()+",");
				}
			}
		}
		sb.deleteCharAt(sb.length()-1);
		if(clazz.isAnnotationPresent(Table.class)){
			Table t=(Table) clazz.getAnnotation(Table.class);
			sb.append(" from "+t.table());
		}else{
			sb.append(" from "+clazz.getSimpleName());
		}
		return sb.toString();
	}
	
	/**
	 * 获取查询语句sql
	 * */
	public String getfindByIDSql(String id){
		StringBuilder sb=new StringBuilder();
		sb.append(getSelectHeaderSql());
		sb.append(" where ");
		try {
			Field[] fields=clazz.getDeclaredFields();
			for (Field field : fields) {
				if(field.isAnnotationPresent(ID.class)){
					if(field.isAnnotationPresent(Column.class)){
						Column column=field.getAnnotation(Column.class);
						sb.append(column.name()+" = " + id );
					}else{
						sb.append(field.getName()+" = " + id );
					}
					break;
				} 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}  
		return sb.toString();
	}
	
	/**
	 * 获取查询语句sql
	 * */
	public String getByParamsSql(T t) {
		// TODO Auto-generated method stub
		StringBuffer sb=new StringBuffer();
		sb.append(getSelectHeaderSql()+" where");
		Field[] fields=t.getClass().getDeclaredFields();
		for (Field field : fields) {
			boolean bool=field.isAccessible();
			field.setAccessible(true);
			try {
				if(field.get(t)==null){
					field.setAccessible(bool);
					continue;
				}
				if(!field.isAnnotationPresent(Ignore.class)){
					if(field.isAnnotationPresent(Column.class)){
						Column column=field.getAnnotation(Column.class);
						sb.append(" "+column.name()+"="+field.get(t)+" and ");
					}else{
						sb.append(" "+field.getName()+"="+field.get(t)+" and ");
					}
				}
				field.setAccessible(bool);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String sql=sb.substring(0, sb.length()-4);
		return sql;
	}
	
	/**
	 * 获取要插入的值
	 * */
	public String getInsertValuesSql(T t){
		StringBuilder sb=new StringBuilder();
		sb.append("(");
		try {
			Field[] fields=t.getClass().getDeclaredFields();
			for (Field field : fields) {
				if(field.isAnnotationPresent(Ignore.class)){continue;}
				field.setAccessible(true);
				sb.append("'"+field.get(t)+"',");
				field.setAccessible(false);
			}
			sb.deleteCharAt(sb.length()-1);
			sb.append(")");
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	/**
	 * 获取删除的值
	 * */
	public String getDeleteSql(T t){
		StringBuilder sb=new StringBuilder();
		sb.append("delete from ");
		try {
			Class<?> clazz = t.getClass();
			if(clazz.isAnnotationPresent(Table.class)){
				Table table=(Table) clazz.getAnnotation(Table.class);
				sb.append(table.table());
			}else{
				sb.append(clazz.getSimpleName());
			}
			sb.append(" where ");
			Field[] fields=t.getClass().getDeclaredFields();
			for (Field field : fields) {
				boolean flag=field.isAccessible();
				field.setAccessible(true);
				if(field.get(t)==null||field.isAnnotationPresent(Ignore.class)){
						continue;
				}
				if(field.isAnnotationPresent(Column.class)){
					Column column=field.getAnnotation(Column.class);
					sb.append(column.name()+" = " + field.get(t)+",");
				}else{
					sb.append(field.getName()+" = " + field.get(t)+",");
				}
				field.setAccessible(flag);
			}
			sb.deleteCharAt(sb.length()-1);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb.toString();
	}
	/**
	 * 获取更新语句
	 * */
	public String getUpdataSql(T t,boolean bool){
		StringBuilder sb=new StringBuilder();
		sb.append("update ");
		Class<?> clazz = t.getClass();
		if(clazz.isAnnotationPresent(Table.class)){
			Table table=(Table) clazz.getAnnotation(Table.class);
			sb.append(table.table());
		}
		StringBuilder where=new StringBuilder();
		sb.append(" set ");
		try {
			Field[] fields=t.getClass().getDeclaredFields();
			for (Field field : fields) {
				boolean flag = field.isAccessible();
				field.setAccessible(true);
				if(field.isAnnotationPresent(Ignore.class)){
					continue;
				}
				if(field.isAnnotationPresent(ID.class)){
					if(field.get(t)==null){
						return null;
					}
					if(field.isAnnotationPresent(Column.class)){
						Column column=field.getAnnotation(Column.class);
						where.append(column.name()+" = " + field.get(t)+",");
					}else{
						where.append(" where "+field.getName()+" = " + field.get(t)+",");
					}
				}
				if(bool){
					if(field.get(t)==null){
						continue;
					}
				}
				if(field.isAnnotationPresent(Column.class)){
					Column column=field.getAnnotation(Column.class);
					sb.append(column.name()+" = " + field.get(t)+",");
				}else{
					sb.append(field.getName()+" = " + field.get(t)+",");
				}
				field.setAccessible(flag);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		sb.deleteCharAt(sb.length()-1);
		sb.append(where);
		return sb.toString();
	}
	public String getConditionSql(T t,List<ConditionParams> list){
		StringBuilder sb=new StringBuilder();
		sb.append(getByParamsSql(t));
		sb.append(getConditionSql(list));
		return sb.toString();
	}
	/**获取连接规则**/
	public String getConditionSql(List<ConditionParams> list){
		StringBuilder sb=new StringBuilder();
		for (ConditionParams condition : list) {
			if(condition.getAfterLink().equals("ORDERBY")){
				sb.append(condition.getAfterLink());
				sb.append(condition.getField());
				sb.append(condition.getValue());
				break;
			}
			sb.append(" "+condition.getBeforeLink());
			sb.append(condition.getField());
			sb.append(condition.getAfterLink());
			if(condition.getAfterLink().equals("=")){
				sb.append(condition.getValue());
				break;
			}
			if(condition.getAfterLink().equals("IN")){
				if(condition.getList()!=null){
					sb.append("(");
					for (String str : condition.getList()) {
						sb.append(str+",");
					}
					sb.deleteCharAt(sb.length()-1);
					sb.append(")");
				}else{
					return null;
				}
				break;
			}
			if(condition.getAfterLink().equals("LIKE")){
				if(condition.getList()!=null){
					sb.append("'%");
					sb.append(condition.getValue());
					sb.append("%'");
				}else{
					return null;
				}
				break;
			}
		}
		return sb.toString();
	}
	 
}
