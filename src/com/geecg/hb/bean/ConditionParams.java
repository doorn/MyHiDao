package com.geecg.hb.bean;

import java.util.List;

public class ConditionParams {
	/**查询列**/
	private String field;
	/** 查询列表**/
	private List<String> list ;
	
	public final static String AND="AND";
	
	public final static String OR="OR";
	
	private String beforeLink=AND;//true:and, false:or
	
	public final static String EQUALES="=";
	
	public final static String IN="IN";
	
	public final static String LIKE="LIKE";
	
	public final static String ORDERBY="ORDER By";
	
	public final static String DESC="DESC";
	
	public final static String ASC="ASC";
	
	public final static String GROUPBY="GROUPBY";

	private String afterLink=EQUALES;
	
	private String value;
	public void setValue(String value) {
		this.value = value;
	}
	public String getValue() {
		return value;
	}
	public ConditionParams(){
		super();
	}
	
	public void setField(String field) {
		this.field = field;
	}
	
	public String getField() {
		return field;
	}
	
	public List<String> getList() {
		return list;
	}
	
	public String getBeforeLink() {
		return beforeLink;
	}
	
	public void setBeforeConnector(String link) {
		this.beforeLink = link;
	}
	
	public String getAfterLink() {
		return afterLink;
	}
	
	public void setAfterLink(String afterLink) {
		this.afterLink = afterLink;
	}
	
	public void setList(List<String> list) {
		this.list = list;
	}
	
}
