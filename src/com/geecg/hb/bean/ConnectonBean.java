package com.geecg.hb.bean;

public class ConnectonBean {
	
	public ConnectonBean(){
		super();
	}
	public ConnectonBean(String driverClassName,String username,String password,String url){
		this.driverClassName=driverClassName;
		this.username=username;
		this.password=password;
		this.url=url;
	}
	//数据库驱动类名
	private String driverClassName;
	//用户名称
	private String username;
	//用户密码
	private String password;
	//URL
	private String url;
	
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUrl() {
		return url;
	}
	public String getDriverClassName() {
		return driverClassName;
	}
	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
