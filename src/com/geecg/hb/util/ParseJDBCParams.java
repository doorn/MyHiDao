package com.geecg.hb.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import com.geecg.hb.bean.ConnectonBean;

public class ParseJDBCParams {
	private static ParseJDBCParams psJdbcxml=new ParseJDBCParams();
	private ParseJDBCParams(){}
	public static ParseJDBCParams getParseJDBCXML(){
		return psJdbcxml;
	}
	public static ConnectonBean perseXML(String filePath){
		if(filePath==null){
			filePath="src/resource/application.properties";
		}
		ConnectonBean bean=null;
        try {
        	Properties properties = new Properties();
            InputStream in = new FileInputStream(filePath);
            properties.load(in);
            bean=new ConnectonBean(
            			(String)properties.get("driverClassName"),
            			(String)properties.get("username"),
            			(String)properties.get("password"),
            			(String)properties.get("url"));
            in.close();
        } catch (Exception e) {
        	e.printStackTrace();
        }
		return bean;
	}
}

