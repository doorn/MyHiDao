package test.user.dao;

import java.util.ArrayList;
import java.util.List;

import com.geecg.hb.asqlhandle.SqlHandleImpl;
import com.geecg.hb.bean.ConditionParams;
import com.geecg.hb.dbhandle.HandleDB;

import test.user.modal.User;

public class UserDao extends SqlHandleImpl<User,String>  {
	
	public static void main(String[] args) {
		HandleDB<User> hdb=new HandleDB<User>();
		UserDao dao = new UserDao();
//		dao.insert();
//		dao.saveAll();
//		dao.query();
		dao.update();
//		dao.delete();
	}
	public void insert(){
		User user=new User();
		user.setId("110000");
		user.setAge("10");
		user.setName("00");
		user.setSex("nan");
		add(user);
	}
	
	public void saveAll(){
		List<User> list=new ArrayList<User>();
		User user=new User();
		user.setId("110003");
		user.setAge("10");
		user.setName("00");
		user.setSex("nan");
		User user1=new User();
		user1.setId("110004");
		user1.setAge("10");
		user1.setName("00");
		user1.setSex("nan");
		list.add(user);
		list.add(user1);
		saveAll(list);
	}
	
	public void query(){
		User user=new User();
		user.setAge("10");
		List<User> list=findByParams(user);
		for (User user2 : list) {
			System.out.println(user2.toString());
		}
	}
	public void update(){
		User user=new User();
		user.setAge("20");
		update(user,false);
	}
	public void delete(){
		User user=new User();
		user.setId("110004");
		delete(user);
	}
	
	
	
}
